#!/usr/bin/python3
# -*- coding: utf-8 -*-



#Imports
from bs4 import BeautifulSoup
#https://docs.python.org/3/library/argparse.html
import argparse
import pycurl
import sys
import os
import random
import re
from termcolor import colored
try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO
from urllib.parse import urlparse
import time
import threading
#https://docs.python.org/3.7/library/mmap.html
import mmap

#Variables and classes

dictionary= []

userAgents = [
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:74.0) Gecko/20100101 Firefox/74.0",
	"Mozilla/5.0 (Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36 OPR/67.0.3575.115",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36 OPR/67.0.3575.115",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36 OPR/67.0.3575.115",
	"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36 Vivaldi/2.11",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36 Vivaldi/2.11",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36 Vivaldi/2.11"
]
userAgent = userAgents[random.randint(0,len(userAgents)-1)]

class Spider(object):
	def __init__(self):
		self.lock= threading.Lock()
		self.toCrawl= []
		self.routes = []
		self.css =[]
		self.javascript =[]
		self.sources = []
		self.forms = []
		self.phones = []
		self.mails = []
		self.total = 0
	def getRoute(self,route):
		self.toCrawl
		self.lock.acquire()
		try:
			nextRoute= self.toCrawl.pop(self.toCrawl.index(route))
		except:
			nextRoute=""
		finally:
			self.lock.release()
			return nextRoute
	def newRoute(self,newRoute):
		self.routes
		self.lock.acquire()
		try:
			if newRoute not in self.routes:
				self.routes.append(newRoute)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()
	def newRouteToCrawl(self,newRoute,url):
		self.routes
		self.lock.acquire()
		try:
			if newRoute not in self.routes:
				self.routes.append(newRoute)
				if re.search(url,newRoute) and not re.search(r'.(pdf|doc[x]?)$',newRoute):
					self.toCrawl.append(newRoute)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()
	def newCSS(self,newRoute):
		self.css
		self.lock.acquire()
		try:
			if newRoute not in self.css:
				self.css.append(newRoute)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()
	def newJavaScript(self,newRoute):
		self.javascript
		self.lock.acquire()
		try:
			if newRoute not in self.javascript:
				self.javascript.append(newRoute)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()
	def newSource(self,newRoute):
		self.sources
		self.lock.acquire()
		try:
			if newRoute not in self.sources:
				self.sources.append(newRoute)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()			
	def newPhone(self,phone):
		self.phones
		self.lock.acquire()
		try:
			if phone not in self.phones:
				self.phones.append(phone)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()
	def newMail(self,mail):
		self.mails
		self.lock.acquire()
		try:
			if mail not in self.mails:
				self.mails.append(mail)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()
	def newForm(self,form):
		self.forms
		self.lock.acquire()
		try:
			if form not in self.forms:
				self.forms.append(form)
		except Exception as e:
			#raise e
			pass
		finally:
			self.lock.release()
	def printRoutes(self):
		print("\n")
		self.routes.sort()
		print("\nObtenidas %d rutas nuevas" %self.getTotal())
		for route in range(len(self.routes)):
			print(colored("["+str(route+1)+"]","green").ljust(14," "),self.routes[route])
		print(colored("--------------------------\n","blue"))
	def printMisc(self):
		print("\n")
		self.css.sort()
		self.javascript.sort()
		self.sources.sort()
		self.forms.sort()
		self.mails.sort()
		self.phones.sort()
		print(colored("[*]","yellow"),"Obtenidas %d rutas de hojas de estilos (CSS)." %len(self.css))
		for link in range(len(self.css)):
			print(colored("["+str(link+1)+"]","green").ljust(14," "),self.css[link])
		print(colored("--------------------------\n","blue"))
		print(colored("[*]","yellow"),"Obtenidas %d rutas de librerías JavaScript." %len(self.javascript))
		for script in range(len(self.javascript)):
			print(colored("["+str(script+1)+"]","green").ljust(14," "),self.javascript[script])
		print(colored("--------------------------\n","blue"))
		print(colored("[*]","yellow"),"Obtenidas %d rutas de recursos." %len(self.sources))
		for source in range(len(self.sources)):
			print(colored("["+str(source+1)+"]","green").ljust(14," "),self.sources[source])
		print(colored("--------------------------\n","blue"))
		print(colored("[*]","yellow"),"Obtenidas %d rutas de formularios." %len(self.forms))
		for form in range(len(self.forms)):
			print(colored("["+str(form+1)+"]","green").ljust(14," "),self.forms[form])
		print(colored("[!] NO se realiza un fuzzing a los formularios para evitar posibles problemas.","red"))
		print(colored("--------------------------\n","blue"))
		print(colored("[*]","yellow"),"Obtenidos %d correos electrónicos." %len(self.mails))
		for mail in range(len(self.mails)):
			print(colored("["+str(mail+1)+"]","green").ljust(14," "),self.mails[mail])
		print(colored("--------------------------\n","blue"))
		print(colored("[*]","yellow"),"Obtenidos %d números de teléfono." %len(self.phones))
		for phone in range(len(self.phones)):
			print(colored("["+str(phone+1)+"]","green").ljust(14," "),self.phones[phone])
		print(colored("--------------------------\n","blue"))
	def getTotal(self):
		self.lock.acquire()
		self.total= len(self.routes)
		self.lock.release()
		return self.total
#Functions
def header():
	print("""
	 _____  _____          ______ __  __       
	|____ \|  __ \   /\   |  ____|  \/  |      
	 _   | | |__) | /  \  | |__  | \  / | ___  
	|_|  | |  _  / / /\ \ |  __| | |\/| |/ _ \ 
	 ____| | | \ \/ ____ \| |    | |  | |  __/ 
	|_____/|_|  \/_/    \_\_|    |_|  | |\___| 
	                                  | |      
	                    This is n0t4u |_|      
	"""),

def curlRequest(url):
	#routes[url]=1
	try:	
		buffer= BytesIO()
		c= pycurl.Curl()
		c.setopt(c.URL, url)
		c.setopt(c.FOLLOWLOCATION, True)
		#c.setopt(pycurl.MAXREDIRS, 5)
		c.setopt(c.WRITEFUNCTION,buffer.write)
		c.setopt(pycurl.USERAGENT,userAgent) #Cambiar si se añaden más UserAgents
		c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
		c.setopt(pycurl.COOKIEFILE, 'cookie.txt')
		c.perform()
		c.close()
		
		body= buffer.getvalue()
		return body.decode('iso-8859-1')
	except Exception as e:
		"""if e.args[0]==6:
			sys.exit(e.args[1])
		else:
			raise e"""
		return False

def curlLoginRequest(url,credentials,csrf):
	print("POST REQUEST")
	try:	
		buffer= BytesIO()
		c= pycurl.Curl()
		c.setopt(c.URL, url)
		c.setopt(c.FOLLOWLOCATION, True)
		#c.setopt(pycurl.MAXREDIRS, 5)
		c.setopt(c.WRITEFUNCTION,buffer.write)
		c.setopt(pycurl.USERAGENT,userAgent) #Cambiar si se añaden más UserAgents
		c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
		c.setopt(pycurl.COOKIEFILE, 'cookie.txt')
		c.setopt(pycurl.POST, True)
		if csrf != None:
			credentials = credentials.replace("$",csrf)
			print(colored(credentials,"yellow"))
		c.setopt(pycurl.POSTFIELDS, credentials)
		c.perform()
		c.close()
		
		body= buffer.getvalue()
		f = open("cookie.txt","r")
		print(body.decode('iso-8859-1'))
		return body.decode('iso-8859-1')
	except Exception as e:
		"""if e.args[0]==6:
			sys.exit(e.args[1])
		else:
			raise e"""
		return False

def checkCSRF(html):
	soup=BeautifulSoup(html,'html.parser')
	for hiddenInput in soup.find_all("input", type="hidden"):
		print(hiddenInput)
		if re.search(r'(token|login|csrf)',str(hiddenInput)):
			print(colored(hiddenInput.get("value"),"red"))
			return hiddenInput.get("value")

def getURLs(html,url):
	soup=BeautifulSoup(html,'html.parser')
	for link in soup.find_all("a"):
		newRoute= link.get("href")
		if newRoute and len(newRoute) >1:
			if re.search(r'[#?]',newRoute):
				newRoute= re.split(r'[#?]',newRoute)[0]
			elif re.search(r'javascript:',newRoute):
				if re.search(r'javascript:(next|window\.open)\([\"\']{1}',newRoute):
					newRoute = re.split(r'javascript:(next|window\.open)\([\"\']{1}',newRoute)[1]
				else:
					continue
			elif re.search(r'mailto:',newRoute):
				newRoute,mail = re.split(r'mailto:',newRoute,1)
				#Añadir comprobación if args...
				spider.newMail(mail)
			elif re.search(r'tel:',newRoute):
				newRoute,phone=re.split(r'tel:',newRoute,1)
				#Añadir comprobación if args...
				spider.newPhone(phone)
			elif re.search(r'[\.\/]{1}$',newRoute):
				if re.search(r'[\.\/]{2}$',newRoute):
					newRoute = newRoute[:-2]
				else:	
					newRoute = newRoute[:-1]
			if not re.search(r'http[s]?\:\/\/',newRoute):
				newRoute= rootDomain.scheme+"://"+rootDomain.netloc+newRoute
			newRoute = newRoute.replace(" ","%20")
			spider.newRoute(newRoute)
	if args.all:
		getMisc(soup,url)
		getForms(soup)

def getURLsRecursive(html,url):
	soup=BeautifulSoup(html,'html.parser')
	for link in soup.find_all("a"):
		newRoute= link.get("href")
		if newRoute and len(newRoute) >1:
			if re.search(r'[#?]',newRoute):
				newRoute= re.split(r'[#?]',newRoute)[0]
			elif re.search(r'javascript:',newRoute):
				if re.search(r'javascript:(next|window\.open)\([\"\']{1}',newRoute):
					newRoute = re.split(r'javascript:(next|window\.open)\([\"\']{1}',newRoute)[1]
				else:
					continue
			elif re.search(r'mailto:',newRoute):
				newRoute,mail = re.split(r'mailto:',newRoute,1)
				#Añadir comprobación if args...
				spider.newMail(mail)
			elif re.search(r'tel:',newRoute):
				newRoute,phone=re.split(r'tel:',newRoute,1)
				#Añadir comprobación if args...
				phone= phone.replace(" ","")
				spider.newPhone(phone)
			elif re.search(r'[\.\/]{1}$',newRoute):
				if re.search(r'[\.\/]{2}$',newRoute):
					newRoute = newRoute[:-2]
				else:	
					newRoute = newRoute[:-1]
			if not re.search(r'http[s]?\:\/\/',newRoute):
				newRoute= url+"/"+newRoute.strip("/")
			newRoute = newRoute.replace(" ","%20")
			spider.newRouteToCrawl(newRoute, url)
	if args.all:
		getMisc(soup,url)
		getForms(soup)
	
def getMisc(soup,url):
	for link in soup.find_all("link"):
		newRoute=link.get("href")
		if newRoute and re.search(r'\.css',newRoute):
			if re.search(r'[#?]',newRoute):
				#Quitar el parametro del path pero recuperar el resto del path a continuación
				newRoute,aux= re.split(r'[#?]',newRoute,maxsplit=1)
				if aux and len(re.split('/',aux,maxsplit=1)) ==2 :
					newRoute = newRoute+"/"+re.split(r'/',aux,maxsplit=1)[1]
			if not re.search(r'http[s]?\:\/\/',newRoute):
				newRoute= url+"/"+newRoute.lstrip("/")
			newRoute = newRoute.replace(" ","%20")
			spider.newCSS(newRoute)
		elif newRoute and re.search(r'\.ico',newRoute):
			if not re.search(r'http[s]?\:\/\/',newRoute):
				newRoute= url+"/"+newRoute.lstrip("/")
			newRoute = newRoute.replace(" ","%20")
			spider.newSource(newRoute)

	for link in soup.find_all("script"):
		if link.get("rel") == "canonical":
			continue
		newRoute=link.get("src")
		if newRoute:
			if re.search(r'[#?]',newRoute):
				newRoute,aux= re.split(r'[#?]',newRoute,maxsplit=1)
				if aux and len(re.split('/',aux,maxsplit=1)) ==2 :
					newRoute = newRoute+"/"+re.split(r'/',aux,1)[1]
			if not re.search(r'http[s]?\:\/\/',newRoute):
				newRoute = url+"/"+newRoute.lstrip("/")
			newRoute = newRoute.replace(" ","%20")
			spider.newJavaScript(newRoute)
	for link in soup.find_all("img"):
		newRoute=link.get("src")
		if newRoute:
			if re.search(r'[#?]',newRoute):
				newRoute,aux= re.split(r'[#?]',newRoute,maxsplit=1)
				try:
					newRoute = newRoute+"/"+re.split(r'/',aux,1)[1]
				except:
					continue
			if not re.search(r'http[s]?\:\/\/',newRoute):
				newRoute= url+"/"+newRoute.lstrip("/")
			newRoute = newRoute.replace(" ","%20")
			spider.newSource(newRoute)

def getForms(soup):
	for form in soup.find_all("form"):
		newForm= form.get("action")
		if newForm and newForm != "#":
			spider.newForm(newForm.lstrip("."))


def printRoutesRecursive(route, number):
	print("Ruta: %s" %route)
	number = len(routes)-number
	print("Obtenidas %d rutas nuevas" %number)
	return number

def recursiveCrawl(spider,url):
	for i in range(args.recursive):
		for route in spider.toCrawl:
			nextRoute= spider.getRoute(route)
			html= curlRequest(nextRoute)
			time.sleep(args.sleep[0]/1000)
			if html:
				getURLsRecursive(html,url)

def recursiveCrawlVerbose(spider,url):
	for i in range(args.recursive):
		for route in spider.toCrawl:
			totalBefore=spider.getTotal()
			nextRoute= spider.getRoute(route)
			html= curlRequest(nextRoute)
			time.sleep(args.sleep[0]/1000)
			if html:
				getURLsRecursive(html,url)
				#spider.setTotal()
				print("Obtenidas %d rutas nuevas, total %d" %((spider.getTotal()-totalBefore),spider.getTotal()))

def generateDictionary():
	globalList = spider.routes +spider.css +spider.javascript +spider.sources
	for route in globalList:
		directories = route.lstrip("https://").lstrip("http://").split("/")
		directories.pop(0)
		for d in directories:
			if not d in dictionary and not re.search(r'.(pdf|doc[x]?|html|asp[x]?|php|jp[e]?g|png|svg|js|css|ico)$',d):
				dictionary.append(d)
	dictionary.sort()


#Main
parser= argparse.ArgumentParser(description="Don't Run Away From Me es un crawler para obtener de forma automática todos los directorios de una URL.")

parser.add_argument("url",help="URL para comenzar el crawling.",nargs=1)
parser.add_argument("-r","--recursive",dest="recursive",help="Búsqueda recursiva en cada ruta encontrada.",type=int,choices=range(1,6), default=0)
parser.add_argument("-s","--sleep",dest="sleep",help="Tiempo, en milisegundos, de espera entre peticiones.",type=int,nargs=1, default=[0])
#parser.add_argument("-t","--threads",dest="threads",help="Hebras durante la ejecución.",type=int,choices=range(2,11),default=2)
parser.add_argument("-l","--login",dest="login",help="Credenciales para iniciar sesión.Usar $ para los token CSRF.\n Ej: \"username=n0t4u&pwd=n0t4u&csrf_token=$\"", nargs=1, default=[0])
parser.add_argument("-v","--verbose",dest="verbose", help="Muestra por pantalla tramas de la ejecución.", action="store_true")
parser.add_argument("-o","--output",dest="output",help="Fichero de salida de los resultados", nargs = 1)

urlsgroup= parser.add_mutually_exclusive_group()
urlsgroup.add_argument("-u","--urls",dest="urls",help="Mostrar URLs obtenidas por pantalla.",action="store_true")
urlsgroup.add_argument("-a","--all",dest="all",help="Mostrar todas las URLs y recursos obtenidos por pantalla.",action="store_true")

dictgroup = parser.add_mutually_exclusive_group()
dictgroup.add_argument("-d","--dictionary",dest="dictionary", help="Generación de diccionario de directorios e impresión en pantalla.", action="store_true")
dictgroup.add_argument("-do","--dOutput",dest="dOutput", help="Generación de diccionario de directorios en el fichero especificado", nargs=1)
parser.add_argument("--check",dest="check", help="Comprueba en el diccionario dado y añade la palabra si no existe.", action="store_true")

args = parser.parse_args()

if __name__ == '__main__':
	
	header()
	startTime = time.time()
	spider = Spider()
	spider.newRoute(args.url[0])
	rootDomain= urlparse(args.url[0])
	if not rootDomain.scheme:
		sys.exit(colored("Debe indicar una dirección web completa. https://example.com","red"))
	print(colored("[»] Iniciando crawling...","green"))
	if args.login[0]:
		getResponse=curlRequest(args.url[0])
		csrf= checkCSRF(getResponse)
		html=curlLoginRequest(args.url[0],args.login[0],csrf)
	else:
		html= curlRequest(args.url[0])
	if rootDomain[2]:
		url = rootDomain.scheme+"://"+rootDomain.netloc+"/"+rootDomain.path.strip("/")
	else:
		url = rootDomain.scheme+"://"+rootDomain.netloc.rstrip("/")
	if args.recursive:
		getURLsRecursive(html,url)
		if args.verbose:
			try:
				hilo1= threading.Thread(target=recursiveCrawlVerbose,args=(spider,url),name="Hilo1",daemon=True)
				hilo2= threading.Thread(target=recursiveCrawlVerbose,args=(spider,url),name="Hilo2",daemon=True)
				hilo3= threading.Thread(target=recursiveCrawlVerbose,args=(spider,url),name="Hilo3",daemon=True)
				hilo4= threading.Thread(target=recursiveCrawlVerbose,args=(spider,url),name="Hilo4",daemon=True)
				hilo1.start()
				hilo2.start()
				hilo3.start()
				hilo4.start()
				hilo1.join()
				hilo2.join()
				hilo3.join()
				hilo4.join()
			except KeyboardInterrupt:
				#CTRL+C
				sys.exit(colored("Abortando crawling...","red"))
		else:
			try:
				hilo1= threading.Thread(target=recursiveCrawl,args=(spider,url),name="Hilo1",daemon=True)
				hilo2= threading.Thread(target=recursiveCrawl,args=(spider,url),name="Hilo2",daemon=True)
				hilo3= threading.Thread(target=recursiveCrawl,args=(spider,url),name="Hilo3",daemon=True)
				hilo4= threading.Thread(target=recursiveCrawl,args=(spider,url),name="Hilo4", daemon=True)
				hilo1.start()
				hilo2.start()
				hilo3.start()
				hilo4.start()
				hilo1.join()
				hilo2.join()
				hilo3.join()
				hilo4.join()
			except KeyboardInterrupt:
				#CTRL+C
				sys.exit(colored("Abortando crawling...","red"))
	else:
		if html:
			getURLs(html,url)
	print(colored("[»] Crawling finalizado","green"))
	if args.dictionary:
		print("Generando diccionario de directorios...")
		generateDictionary()
		for word in dictionary:
			print(word)
		print("\nEncontrados %d directorios diferentes" % len(dictionary))
	if args.dOutput:
		print("Generando diccionario de directorios...")
		generateDictionary()
		if args.check:
			try:
				print("Se han encontrado %d directorios diferentes" % len(dictionary))
				with open(args.dOutput[0],"a+", encoding="iso-8859-1") as f:
				    s= mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
				    counter =0
				    for word in dictionary:
				    	if s.find(word.encode()) == -1:
				    		f.write(word+"\n")
				    		counter +=1
				    print("Se han añadido al diccionario %s %d palabras" %(args.dOutput[0],counter))
			except Exception as e:
				if e.args[0]==13:
					sys.exit(args.dOutput[0] +" "+ colored(e.args[1],"red"))
				else:
					print(colored("[ERROR]","red"),"El fichero proporcionado no existe.")
		else:
			try:
				f = open(args.dOutput[0]+".txt","a+", encoding="iso-8859-1")
			except:
				f = open(args.dOutput[0]+".txt","w+", encoding="iso-8859-1")
			finally:
				for word in dictionary:
					f.write(word+"\n")
				f.close()
				print("\nDiccionario creado con %d directorios nuevos." % len(dictionary))
	if args.output:
		print("Archivo %s.txt creado" %args.output[0])
		f = open(args.output[0]+".txt","w", encoding="iso-8859-1") #Cambiar por "a"
		globalList = spider.routes +spider.css +spider.javascript +spider.sources
		for route in globalList:
			f.write(route+"\n") #route.enconde("iso-8859-1")
		f.close()
		print("\nObtenidas %d rutas." %spider.getTotal())
	if args.urls:
		spider.printRoutes()
	elif args.all:
		spider.printRoutes()
		spider.printMisc()
	os.remove("cookie.txt")
	executionTime = time.time()- startTime
	if executionTime/60 > 1:
		print("--- %s segundos --- (%d minutos) ---" %(round(executionTime,3), round(executionTime/60,3)))
	else:
		print("--- %s segundos ---" %round(executionTime,3))